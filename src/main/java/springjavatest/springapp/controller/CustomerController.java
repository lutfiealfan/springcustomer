package springjavatest.springapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springjavatest.springapp.mysql.model.Customer;
import springjavatest.springapp.service.CustomerResponse;
import springjavatest.springapp.service.CustomerService;

import java.util.List;
import java.util.stream.Stream;

@RestController
public class CustomerController {
    @Autowired
    CustomerService customerService;

    @PostMapping(value = "/customer")
    public ResponseEntity<?> addCustomer(@RequestBody Customer customer) {
        String response = customerService.addCustomer(customer);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/customer")
    public Stream<CustomerResponse> getCustomer() {
        List<Customer>  c = customerService.getCustomer();
        return c.stream().map(a -> new CustomerResponse().toResponse(a.getId(), a.getNama(), a.getUmur(), a.getAgama(), a.getAlamat()));
    }

    @GetMapping(value = "/customer/{id}")
    public Customer getCustomerById(@PathVariable(value = "id") int id) {
        return customerService.getCustomerById(id);
    }

    @PutMapping(value = "/customer/{id}")
    public ResponseEntity<?> updateCustomer(@PathVariable(value = "id") int id,
                                            @RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.updateCustomer(id, customer));
    }

    @DeleteMapping(value = "/customer/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable(value = "id") int id) {
        return ResponseEntity.ok(customerService.deleteCustomer(id));
    }

}
