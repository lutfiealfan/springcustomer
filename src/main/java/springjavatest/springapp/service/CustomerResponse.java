package springjavatest.springapp.service;

public class CustomerResponse {
    private int Id;
    private String namaCustomer;
    private String umurCustomer;
    private String agamaCustomer;
    private String alamatCustomer;

    public CustomerResponse toResponse(int id, String namaCustomer, String umurCustomer, String agamaCustomer, String alamatCustomer) {
        CustomerResponse cs = new CustomerResponse();
        cs.setId(id);
        cs.setNamaCustomer(namaCustomer);
        cs.setUmurCustomer(umurCustomer);
        cs.setAgamaCustomer(agamaCustomer);
        cs.setAlamatCustomer(alamatCustomer);

        return cs;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNamaCustomer() {
        return namaCustomer;
    }

    public void setNamaCustomer(String namaCustomer) {
        this.namaCustomer = namaCustomer;
    }

    public String getUmurCustomer() {
        return umurCustomer;
    }

    public void setUmurCustomer(String umurCustomer) {
        this.umurCustomer = umurCustomer;
    }

    public String getAgamaCustomer() {
        return agamaCustomer;
    }

    public void setAgamaCustomer(String agamaCustomer) {
        this.agamaCustomer = agamaCustomer;
    }

    public String getAlamatCustomer() {
        return alamatCustomer;
    }

    public void setAlamatCustomer(String alamatCustomer) {
        this.alamatCustomer = alamatCustomer;
    }
}
