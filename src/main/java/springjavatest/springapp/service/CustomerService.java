package springjavatest.springapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import springjavatest.springapp.mysql.model.Customer;
import springjavatest.springapp.mysql.repository.CustomerRepository;

import java.util.List;

@Service
public class CustomerService {
    private Logger logger = LoggerFactory.getLogger(CustomerService.class);
    @Autowired
    CustomerRepository customerRepository;

    public List<Customer> getCustomer () {
        List<Customer> customers = customerRepository.getCustomers();

        return customers;
    }

    public String addCustomer (Customer customer) {
        try {
            customerRepository.addCustomer(customer);

            return "Data Has Been Saved";
        } catch (Exception e) {
            logger.error("Error While add data", e);
            return "Error While adding data";
        }
    }

    public String deleteCustomer (int id) {
        try {
            customerRepository.deleteCustomer(id);
            return "Success Deleting data";
        } catch (Exception e) {
            logger.error("Error While Delete Data ", e);
            return "Error While Deleting data";
        }
    }

    public String updateCustomer (int id, Customer customer) {
        try {
            customerRepository.updateCustomer(customer, id);
            return "Success Update data";
        } catch (Exception e) {
            logger.error("Error While Update Data ", e);
            return  "Error While Update Data";
        }
    }

    public Customer getCustomerById(int id) {
        return customerRepository.getCustomerById(id);
    }
}
