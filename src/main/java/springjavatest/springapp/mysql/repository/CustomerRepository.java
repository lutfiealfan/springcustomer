package springjavatest.springapp.mysql.repository;

import springjavatest.springapp.mysql.model.Customer;

import java.util.List;

public interface CustomerRepository{
    List<Customer> getCustomers();
    Customer getCustomerById(int id);
    void addCustomer(Customer customer);
    void updateCustomer(Customer customer, int id);
    void deleteCustomer(int id);
}
