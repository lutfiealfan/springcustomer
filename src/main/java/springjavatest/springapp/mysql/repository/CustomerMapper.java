package springjavatest.springapp.mysql.repository;

import org.springframework.jdbc.core.RowMapper;
import springjavatest.springapp.mysql.model.Customer;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerMapper implements RowMapper<Customer> {
    @Override
    public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Customer customer = new Customer();
        customer.setId(rs.getInt("id"));
        customer.setNama(rs.getString("nama"));
        customer.setUmur(rs.getString("umur"));
        customer.setAlamat(rs.getString("alamat"));
        customer.setAgama(rs.getString("agama"));
        return customer;
    }
}
