package springjavatest.springapp.mysql.repository;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import springjavatest.springapp.mysql.model.Customer;

import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    private JdbcTemplate jdbcTemplate = (JdbcTemplate) context.getBean("jdbcTemplate");

    @Override
    public List<Customer> getCustomers() {
        String sql = "SELECT * FROM `dataCustomer` ";

        return jdbcTemplate.query(sql, new CustomerMapper());
    }

    @Override
    public Customer getCustomerById(int id) {
        String sql = "SELECT * FROM dataCustomer WHERE id=?";

        return jdbcTemplate.query(sql, new CustomerMapper()).get(0);
    }

    @Override
    public void addCustomer(Customer customer) {
        String sqldatCustomer = "INSERT INTO dataCustomer (nama, umur, alamat, agama) VALUES (?, ?, ?, ?)";

        jdbcTemplate.update(sqldatCustomer, customer.getNama(), customer.getUmur(), customer.getAlamat(),
                customer.getAgama());
    }

    @Override
    public void updateCustomer(Customer customer, int id) {
        String sqldatCustomer =  "UPDATE dataCustomer set nama=?, umur=?, alamat=?, agama=? WHERE id=?";

        jdbcTemplate.update(sqldatCustomer, customer.getNama(), customer.getUmur(), customer.getAlamat(), customer.getAgama(),
                customer.getId());
    }

    @Override
    public void deleteCustomer(int id) {
        String sqldatCustomer = "DELETE FROM dataCustomer WHERE id=?";

        jdbcTemplate.update(sqldatCustomer, id);
    }
}
